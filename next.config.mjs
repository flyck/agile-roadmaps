/**
 * Run `build` or `dev` with `SKIP_ENV_VALIDATION` to skip env validation. This is especially useful
 * for Docker builds.
 */
await import("./src/env.mjs");

const isProduction = process.env["NODE_ENV"] === "production"
const repoName = "agile-roadmaps"

/** @type {import("next").NextConfig} */
const config = {
  reactStrictMode: true,
  // fix for gitlab pages https://stackoverflow.com/questions/45141957/how-to-deploy-next-js-app-to-gitlab-github-pages
  assetPrefix: isProduction ? `/${repoName}` : '',
  output: 'export',
  images: {
    unoptimized: true
  },
  /**
   * If you have `experimental: { appDir: true }` set, then you must comment the below `i18n` config
   * out.
   *
   * @see https://github.com/vercel/next.js/issues/41980
   */
  /** i18n: {
     locales: ["en"],
     defaultLocale: "en",
   },*/
};
export default config;
