# Agile Roadmaps

A project aiming at preventing teams from doing waterfall in sprints by bringing more visibility
into the iterative planning process.

It's intended use is in company status meetings, in planning meetings or similar.

Link to [demo](https://flyck.gitlab.io/agile-roadmaps/)

Initial sketch:

![](assets/dashboard_sketch.png)

## Integrations

The agile roadmap is pretty much just a normal visualization of what you can typically expect from
an agile Project. It depends on certain agile building blocks, which should be pulled from the
used planning tool. Those include:
- `Project Plan`
  - Implementations:
    - Wiki Page
    - Epic? It could be argued that a plan is pretty much an epic. The problem with epics is that
      we generally cannot version them. Epics are often a superset of features or a group of
      features / action items that together make a big achievable, like a milestone. Sometimes
      epics are just topics of similar tasks. Often Epics will have the company KPIs they relate
      to mentioned in their description.
- `Project Plan Version`
  - In order to show the impact of iterations on the project plan, we need proper project plan
    versions.
  - Implementation: Named versions in the wiki history or simply new pages
- `Hypotheses`
  - Status: Open, Confirmed, Disconfirmed, Dropped
  - Start and End date(?)
  - Implementation: Probably tagged issues / tickets or even milestones
- `Customer Feedback`
  - Date when it was received
  - Implementation: Probably a tagged issue or a page in the wiki
- `Learning / Realization / Decision`: A significant piece of information that people want to
  display in the agile roadmap. Might not be used really but is just there for completeness.
  - Date when it was received
  - Implementation: Probably a tagged issue or a page in the wiki
- `Project KPIs`: Projects generally aim at increasing some KPI. Should be possible to display it
  in the board, potentially near the top. Fosters a more data-driven approach.
  - Start and End Date when KPI was considered
  - Implementation: ?


### Needed Components
To integrate the dashboard with whatever planning systems, certain agile building blocks need to
be supported, in order for them to be displayed. Those are:
- User stories
- Epics (optional)
- Sprints (optional once kanban is supported)
- Release
- Project Plan
- Project Plan Version
- Hypothesis


### Github

- User stories: Issues
- Epics:
  - Not natively supported
  - Can be added via "ZenHub" which adds this epic functionality https://unito.io/blog/github-projects-agile/
- Sprints:
  - The "project" feature has sprint as a supercharged custom feature
    - An issue can only be part of a single sprint. The field only allows one value.
  - Milestones probably were used for this thing in the past
    [docs](https://docs.github.com/en/search-github/searching-on-github/searching-issues-and-pull-requests#search-by-missing-metadata)
- Release: Release
- Project Plan: Wiki Page
- Project Plan Version: New Wiki Page
  - Document versions are not supported
  - Project plans in git could be cool, but let's not be asking too much from our PMs at this
    point. Using some kind of wiki will be much closer to the reality.

### Gitlab ☹

The things that make gitlab a jira competitor are all premium features, which are not activated in
my personal repo.

- User stories: [Issues](https://docs.gitlab.com/ee/user/project/issues/)
- Sprints:
  - Iterations are recommended for sprints, but they are a [premium
    feature](https://docs.gitlab.com/ee/user/group/iterations/). In the docs they mention
    themselves they are basically supercharged milestones
- Epics:
  - Epics are also a [premium feature](https://docs.gitlab.com/ee/user/group/epics/)
- Release: Release

### Trello

### Jira 💩
