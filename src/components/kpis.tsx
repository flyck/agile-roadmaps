import React, { useEffect, useState } from 'react';
import Modal from '../layouts/modal';
import Link from 'next/link';
import RootModal from "~/components/helper/modal"
import { calculateTimelinePosition, isRelevantDate } from '~/utils/timeline';
import { DateRange } from '~/types/general';
import { Mode } from './settings';
import { Kpi } from '~/types/kpis';
import { kpis as mockKpis } from '~/components/mockData'
import { GithubService } from '~/services/github';

interface KpiComponentProps {
  dateRange: DateRange
  mode: {
    mode: Mode
    setMode: (mode: Mode) => void
  }
}

interface CoolButtonProps {
  kpi: Kpi
  openModal: (content: Kpi) => void
  dateRange: DateRange
}

const CoolButton: React.FC<CoolButtonProps> = (props) => {
  const startPosition = calculateTimelinePosition(props.dateRange.startDate, props.dateRange.endDate, props.kpi.from)
  const endPosition = calculateTimelinePosition(props.dateRange.startDate, props.dateRange.endDate, props.kpi.to)
  const width = endPosition - startPosition

  return <>
    <div className="relative flex h-auto justify-between normal-case btn btn-xs bg-gray-300 text-black" onClick={() => props.openModal(props.kpi)}
      style={{ left: `${startPosition}%`, width: `${width}%` }}
    >
      <span className={(props.kpi.name ? "col-span-8" : "col-span-10")} >{props.kpi.name}</span>
    </div >
  </>
}

const KpiComponent: React.FC<KpiComponentProps> = ({ dateRange: { startDate, endDate }, mode }) => {
  const [activeModal, setActiveSprintModal] = useState<Kpi>();
  const [loading, setLoading] = useState<boolean>(true);
  const [oldMode, setOldMode] = useState<Mode>(mode.mode)
  const [kpis, setKpis] = useState<Kpi[]>();

  useEffect(() => {
    async function loadData() {
      console.log(mode)
      if (mode.mode == Mode.Mock) {
        setKpis(mockKpis)
      }
      if (mode.mode == Mode.Github) {
        console.log("Loading from github")
        try {
          const githubService = new GithubService()
          const kpis = await githubService.getKpis({ startDate, endDate })
          setKpis(kpis)
        } catch (e) {
          console.error(e)
        }
      }
      setLoading(false)
    }

    if (loading) { loadData() } /* eslint-disable-line @typescript-eslint/no-floating-promises */
    if (oldMode != mode.mode) { console.log("Mode changed, must load again!"); setLoading(true); loadData(); setOldMode(mode.mode) }    /* eslint-disable-line @typescript-eslint/no-floating-promises */
  }, [mode.mode])

  const handleOpenModal = (content: Kpi | undefined) => {
    setActiveSprintModal(content);
  };

  const handleCloseModal = () => {
    setActiveSprintModal(undefined);
  };

  return <>
    <div className="flex">
      <div className="container mx-auto">
        <div className="relative flex flex-col gap-2">
          {kpis?.filter((kpi) => isRelevantDate(startDate, endDate, kpi.from, kpi.to)).map((kpi) => {
            return <CoolButton key={kpi.name} dateRange={{ startDate, endDate }} kpi={kpi} openModal={() => handleOpenModal(kpi)} />
          })}
        </div>
      </div>
    </div>
    <RootModal>
      {activeModal && (
        <Modal
          bgColor="bg-gray-100"
          textColor="text-black"
          onClose={handleCloseModal}
          title={"Kpi: " + activeModal.name}
        >
          {activeModal.url ? <Link target="_blank" href={activeModal.url || ""} className="text-blue-900">Issue</Link> : undefined}
          <div className="">
            Description:
            <p>{activeModal.description}</p>
          </div>
          <br />
          <br />
          <p>Active from {activeModal.from} to {activeModal.to}</p>
          <br />
          Link to <Link href="https://grafana.com">graph</Link>
        </Modal>
      )}
    </RootModal >
  </>
}

export default KpiComponent
