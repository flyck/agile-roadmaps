import Link from "next/link";
import { DateRange } from "~/types/general";
import { calculateTimelinePosition } from "~/utils/timeline"

enum EventType {
  CUSTOMER_FEEDBACK = "Customer Feedback",
  DECISION = "Noteworthy Decision",
  EVENT = "Event",
  KICKOFF = "Kickoff",
  PLAN_RELEASE = "Plan Release"
}

const events = [
  {
    type: EventType.PLAN_RELEASE,
    date: "5.2.2023",
    title: "Product Vison v1",
    link: "https://github.com/flyck/dummy-planning/wiki/Project-Plan-v1"
  },
  {
    type: EventType.CUSTOMER_FEEDBACK,
    date: "5.26.2023",
    title: "Make better downloads!"
  },
  {
    type: EventType.DECISION,
    date: "6.08.2023",
    title: "Lets pivot to be a news-app!"
  },
  {
    type: EventType.PLAN_RELEASE,
    date: "6.10.2023",
    title: "Product Vision v2",
    link: "https://github.com/flyck/dummy-planning/wiki/Project-Plan-v2"
  },
  {
    type: EventType.CUSTOMER_FEEDBACK,
    date: "6.11.2023",
    title: "Great product! Love it.",
    connections: ["Release-v1.0.0"]
  },
  {
    type: EventType.EVENT,
    date: "6.23.2023",
    title: "Kickoff news-app."
  },
]

interface EventProps {
  dateRange: DateRange
  displayDecisions: boolean
  link?: URL;
  connections?: string[]
}

const Events: React.FC<EventProps> = ({ dateRange: { startDate, endDate }, displayDecisions }) => {
  return <>
    <div className={displayDecisions ? "h-72" : "h-60"}>
      {displayDecisions ?
        <div className="relative h-12">
          {
            events
              .filter((event) => ([EventType.DECISION, EventType.KICKOFF, EventType.EVENT].includes(event.type)))
              .map((event, index) => (
                <div
                  key={index}
                  className="absolute text-white whitespace-normal md:whitespace-nowrap"
                  style={{ left: `${calculateTimelinePosition(startDate, endDate, event.date)}%`, bottom: 5 }}
                >
                  <div className="radio bg-yellow-400"></div>
                  {event.title}
                </div>
              ))
          }
        </div>
        : undefined
      }
      <div className="relative h-12 z-0">
        {
          events
            .filter((event) => (event.type == EventType.CUSTOMER_FEEDBACK))
            .map((event, index) => (
              <div
                key={index}
                className="absolute text-white z-0"
                style={{ left: `${calculateTimelinePosition(startDate, endDate, event.date)}%`, bottom: 5 }}
                onClick={() => findConnections(event.connections ?? [])}
              >
                <div className="relative radio bg-yellow-200 z-0"></div>
                {event.title}
              </div>
            ))
        }
      </div>
      <div className="relative h-32 z-0">
        {
          events
            .filter((event) => (event.type == EventType.PLAN_RELEASE))
            .map((event, index) => (
              <div
                key={index}
                className="absolute text-white z-0"
                style={{ left: `${calculateTimelinePosition(startDate, endDate, event.date)}%`, bottom: 5 }}
              >
                {event.link ? <Link href={event.link}>
                  <div className="relative rounded-full bg-blue-400 z-0 h-24 w-24"></div>
                </Link> : <div className="relative rounded-full bg-blue-400 z-0 h-24 w-24"></div>}
                {event.title}
              </div>
            ))
        }
      </div>
    </div >
  </>


}

function findConnections(cons: string[]) {
  if (!cons) console.log("No Connections")

  cons.forEach((connection) => {
    const exists = document.getElementById(connection)
    console.log(exists)
  })
}

export default Events
