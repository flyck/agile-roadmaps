import type React from 'react';
import { useEffect, useRef } from 'react';
import ReactDOM from 'react-dom';

interface ModalProps {
  children: React.ReactNode;
}

const RootModal: React.FC<ModalProps> = ({ children }) => {
  const modalRef = useRef<HTMLDivElement | null>(null);

  useEffect(() => {
    const modalRoot = document.getElementById('modal-root');

    modalRef.current = document.createElement('div');
    modalRoot?.appendChild(modalRef.current);

    return () => {
      modalRoot?.removeChild(modalRef.current!);
      modalRef.current = null;
    };
  }, []);

  if (!modalRef.current) return null;

  return ReactDOM.createPortal(children, modalRef.current);
};

export default RootModal;
