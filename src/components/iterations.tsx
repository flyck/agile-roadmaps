import React, { useEffect, useState } from 'react';
import { calculateTimelinePosition, isRelevantDate } from '~/utils/timeline';
import iterationImg from "~/assets/iteration_2.png"
import footballImg from "~/assets/soccer-player.png"
import Modal from '../layouts/modal';
import { DateRange } from '~/types/general';
import RootModal from './helper/modal';
import Link from 'next/link';
import { Iteration } from '~/types/iterations';
import { Mode } from './settings';
import { GithubService } from '~/services/github';
import { iterations as mockIterations } from '~/components/mockData';


interface TimeSpanWithFlagsProps {
  dateRange: DateRange
  mode: {
    mode: Mode
    setMode: (mode: Mode) => void
  }
}

const IterationsComponent: React.FC<TimeSpanWithFlagsProps> = ({ dateRange: { startDate, endDate }, mode }) => {
  const [activeModal, setActiveModal] = useState<Iteration>();
  const [iterations, setIterations] = useState<Iteration[]>();
  const [loading, setLoading] = useState<boolean>(true);
  const [oldMode, setOldMode] = useState<Mode>(mode.mode)

  const handleOpenModal = (content: Iteration | undefined) => {
    setActiveModal(content);
  };

  const handleCloseModal = () => {
    setActiveModal(undefined);
  };

  useEffect(() => {
    function loadData() {
      console.log(mode)
      if (mode.mode == Mode.Mock) {
        setIterations(mockIterations)
      }
      if (mode.mode == Mode.Github) {
        console.log("Loading from github")
        try {
          const iterations = [] as Iteration[]
          setIterations(iterations)
        } catch (e) {
          console.error(e)
        }
      }
      setLoading(false)
    }

    if (loading) { loadData() } /* eslint-disable-line @typescript-eslint/no-floating-promises */
    if (oldMode != mode.mode) { console.log("Mode changed, must load again!"); setLoading(true); loadData(); setOldMode(mode.mode) }    /* eslint-disable-line @typescript-eslint/no-floating-promises */
  }, [mode.mode])

  const link = (text = "Issue", target?: string) => {
    return <Link target="_blank" href={target || ""} className="text-blue-900">{text}</Link>
  }

  return (
    <>
      <div className="flex h-24">
        <div className="flex-1">
          <div className="relative h-24">
            {iterations?.filter((iteration) => isRelevantDate(startDate, endDate, iteration.date)).map((iteration, index) => (
              <div
                key={"Release-" + index.toString()}
                id={"Release-" + index.toString()}
                className="absolute rounded-lg border-2 border-secondary bg-secondary z-10"
                onClick={() => handleOpenModal(iteration)}
                style={{
                  left: `${calculateTimelinePosition(startDate, endDate, iteration.date)}%`, bottom: 5, width: 70,
                  marginLeft: `${(calculateTimelinePosition(startDate, endDate, iteration.date) / 100) * (-70)}px`
                }}
              >
                <img className="relative" src={iteration.type == "iteration" ? iterationImg.src : footballImg.src} alt="https://www.flaticon.com/free-icons/iteration"></img>
                <div className="relative h-auto badge bg-cyan-200 text-black text-center whitespace-normal md:whitespace-nowrap">{iteration.type == "iteration" ? iteration.id : "Kickoff"}</div>
              </div>
            ))}
            {iterations?.filter((iteration) => isRelevantDate(startDate, endDate, iteration.date)).map((iteration, index, relevantIterations) => {
              if (index == 0) return
              const a = relevantIterations[index - 1]!.date
              const b = relevantIterations[index]!.date
              const diff = calculateTimelinePosition(startDate, endDate, b) - calculateTimelinePosition(startDate, endDate, a)
              return <div
                key={`arrow-${index.toString()}`}
                className="absolute rounded-lg bg-secondary z-0"
                style={{
                  height: "10px",
                  left: `${calculateTimelinePosition(startDate, endDate, a) + 1}%`, bottom: 5, width: `${diff - 1}%`,
                }}>
                <div className="absolute w-6 overflow-hidden inline-block"
                  style={{
                    marginTop: "-13px",
                    left: `100%`,
                    marginLeft: `${(calculateTimelinePosition(startDate, endDate, iteration.date) / 100) * (-70) - 14}px`
                  }}
                >
                  <div className="h-12 bg-secondary rotate-45 transform origin-top-left"></div>
                </div>
              </div>
            })
            }
          </div>
        </div>
      </div >
      <RootModal>
        {activeModal && (
          <Modal
            bgColor="bg-base-100"
            textColor="text-base-content"
            onClose={handleCloseModal}
            title={(activeModal.type == "iteration" ? "Iteration " : "Kickoff ") + activeModal.id}
            titleLink={activeModal.url}
            titleLinkText="Issue"
          >
            <div className="flex flex-col">
              <div className="flex flex-row">
                <div className="flex-1">
                  <b>Description</b>:
                  <p>{activeModal.description}</p>
                </div>
                <div className="w-24 border-4 bg-secondary rounded-lg">
                  <img className="relative" src={activeModal.type == "iteration" ? iterationImg.src : footballImg.src} alt="https://www.flaticon.com/free-icons/iteration"></img>
                  <div className="relative h-auto badge bg-cyan-200 text-black text-center whitespace-normal md:whitespace-nowrap">{activeModal.type == "iteration" ? activeModal.id : "Kickoff"}</div>
                </div>
              </div>
              <div className="mt-4" />
              {activeModal.relatedItems.filter((item) => item.type == "feedback").length > 0 ? <div className="">
                <b>Customer Feedback*</b>:
                <p>{activeModal.relatedItems.filter((item) => item.type == "feedback").map((item, index) => {
                  return <li key={index}>{item.text}</li>
                })}</p>
              </div> : undefined}
              {activeModal.relatedItems.filter((item) => item.type == "release").length > 0 ? <div className="">
                <b>After Release*</b>:
                <p>{activeModal.relatedItems.filter((item) => item.type == "release").map((item, index) => {
                  return <li key={index}>{item.text} ({link("Release", item.url)})</li>
                })}</p>
              </div> : undefined}
              {activeModal.relatedItems.filter((item) => item.type == "kpi").length > 0 ? <div className="">
                <b>Affected KPIs</b>:
                <p>{activeModal.relatedItems.filter((item) => item.type == "kpi").map((item, index) => {
                  return <li key={index}>{item.text} ({link("Kpi", item.url)})</li>
                })}</p>
              </div> : undefined
              }
              {activeModal.relatedItems.filter((item) => item.type == "hypothesis").length > 0 ? <div className="">
                <b>Affected Hypotheses</b>:
                <p>{activeModal.relatedItems.filter((item) => item.type == "hypothesis").map((item, index) => {
                  return <li key={index}>{item.text} ({link("Hypothesis", item.url)})</li>
                })}</p>
              </div> : undefined
              }
              {activeModal.type == "iteration" ? <p className="mt-4">* Having customer feedback and a release are required for an iteration.</p> : undefined}
            </div>
          </Modal>
        )}
      </RootModal >
    </>
  );
};

export default IterationsComponent;
