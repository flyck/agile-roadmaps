import React, { useState } from 'react';
import { calculateTimelinePosition, isRelevantDate } from '~/utils/timeline';
import releaseImg from "~/assets/goal.png"
import { Release } from '~/types/releases';
import { DateRange } from '~/types/general';
import Link from 'next/link';
import RootModal from './helper/modal';
import Modal from '../layouts/modal';

interface TimeSpanWithFlagsProps {
  dateRange: DateRange
  releases: Release[];
}

const Releases: React.FC<TimeSpanWithFlagsProps> = ({ dateRange: { startDate, endDate }, releases }) => {
  const [activeModal, setActiveModal] = useState<Release>();

  const handleOpenModal = (content: Release | undefined) => {
    setActiveModal(content);
  };

  const handleCloseModal = () => {
    setActiveModal(undefined);
  };

  return (
    <>
      <div className="flex h-32">
        <div className="flex-1">
          <div className="relative h-32">
            {releases.filter((release) => isRelevantDate(startDate, endDate, release.date)).map((release) => (
              <div
                key={"Release-" + release.version}
                id={"Release-" + release.version}
                className="absolute"
                onClick={() => handleOpenModal(release)}
                style={{
                  left: `${calculateTimelinePosition(startDate, endDate, release.date)}%`,
                  bottom: 5, width: 70,
                  marginLeft: `${(calculateTimelinePosition(startDate, endDate, release.date) / 100) * (-70)}px`
                }}
              >
                <img className="relative" src={releaseImg.src} alt="https://www.flaticon.com/free-icons/goal"></img>
                <div className="relative h-auto badge bg-yellow-200 text-black text-center whitespace-normal md:whitespace-nowrap">{release.name}</div>
              </div>
            ))}
          </div>
        </div>
      </div>
      <RootModal>
        {activeModal && (
          <Modal
            bgColor="bg-white"
            textColor="text-gray-900"
            onClose={handleCloseModal}
            title={"Release: " + activeModal.name}
            titleLink={activeModal.url ? activeModal.url.toString() : undefined}
            titleLinkText="Link"
          >
            <div className="flex flex-row">
              <div className="flex-1">
                <p>Date: {activeModal.date}</p>
              </div>
              <div className="w-24 rounded-lg">
                <img className="relative" src={releaseImg.src} alt="https://www.flaticon.com/free-icons/iteration"></img>
                <div className="relative h-auto badge bg-cyan-200 text-black text-center whitespace-normal md:whitespace-nowrap">Release </div>
              </div>
            </div>
          </Modal>
        )}
      </RootModal >
    </>
  );
};

export default Releases;
