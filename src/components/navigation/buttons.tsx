
interface SprintNavigationButtonProps {
  onClick: () => void
  displayEpics: boolean
  type: "increase" | "decrease"
}

export const SprintNavigationButton: React.FC<SprintNavigationButtonProps> = (props) => {
  return <div className={
    "absolute bottom-12 hidden sm:flex flex-col justify-end " +
    (props.displayEpics ? "mb-32" : "mb-12") +
    (props.type == "decrease" ? "sm:left-2 left-12" : "sm:right-2 right-12")
  }>
    <button className="btn btn-s w-24 bg-content" onClick={() => props.onClick()}>{props.type == "decrease" ? "«" : "»"}</button>
  </div>

}
