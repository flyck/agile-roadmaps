import React, { ChangeEvent, useState } from 'react';
import github from "~/assets/github-mark.svg"
import jira from "~/assets/jira-mark.svg"
import gitlab from "~/assets/gitlab-mark.svg"
import trello from "~/assets/trello-mark.svg"
import Image from 'next/image';
import RootModal from './helper/modal';
import Modal from '~/layouts/modal';

export enum Mode {
  Mock = "Mocked",
  Github = "Github"
}

const Settings: React.FC<SettingsProps> = ({ epics, events, mode }) => {
  const [isModalOpen, setIsModalOpen] = useState(false);

  const openModal = () => {
    setIsModalOpen(true);
  };

  const closeModal = () => {
    setIsModalOpen(false);
  };


  return (
    <div>
      <button
        className="rounded-full mx-2 mt-2 hover:bg-gray-700 w-6 h-6 text-2xl"
        onClick={openModal}
      >
        ⚙
      </button>

      <RootModal>
        {isModalOpen && (
          <SettingsModal closeModal={closeModal} events={events} epics={epics} mode={mode} />
        )}
      </RootModal>
    </div>
  );
};

interface SettingsProps {
  events: {
    setDisplayDecisions: (input: boolean) => void
    displayDecisions: boolean
  },
  epics: {
    setDisplayEpics: (input: boolean) => void
    displayEpics: boolean
  },
  mode: {
    setMode: (input: Mode) => void
    mode: Mode
  }
}

interface SettingsModalProps extends SettingsProps {
  closeModal: () => void,
}

const SettingsModal: React.FC<SettingsModalProps> = ({ closeModal, events, epics, mode }) => {
  const [integration, setIntegration] = useState<Mode>(mode.mode)

  return (
    <Modal
      title="Settings"
      bgColor="bg-white"
      textColor="white"
      onClose={closeModal}
    >
      {/* Settings content */}
      < b > Integration: Project Management</ b>
      {/* Other settings content */}
      <div className="grid grid-rows-2 mt-2">
        <div className="grid grid-cols-5 text-center">
          <p className="text-center mt-10">🤖</p>
          <div className="text-center flex justify-center items-center">
            <Image
              priority
              src={github as string}
              alt="Github logo"
            />
            {/**  switch? https://duncanleung.com/next-js-typescript-svg-any-module-declaration/ */}
          </div>
          <div className="text-center flex justify-center items-center">
            <Image
              priority
              src={gitlab as string}
              alt="Gitlab logo"
              width="100"
            />
          </div>
          <div className="text-center flex justify-center items-center">
            <Image
              priority
              src={jira as string}
              alt="Jira logo"
              width="100"
            />
          </div>
          <div className="text-center flex justify-center items-center">
            <Image
              priority
              src={trello as string}
              alt="Trello logo"
              width="100"
            />
          </div>          </div>
        <div className="grid grid-cols-5 text-center align-top mt-1">
          <div className="grid grid-rows-2"><p>Mock</p><button className={"btn mx-4" + (mode.mode == Mode.Mock ? " border-4 border-accent" : "")} onClick={() => { setIntegration(Mode.Mock); mode.setMode(Mode.Mock) }}>use</button></div>
          <div className="grid grid-rows-2"><p>Github</p><button className={"btn mx-4" + (mode.mode == Mode.Github ? " border-4 border-accent" : "")} onClick={() => { setIntegration(Mode.Github) }}>use</button></div>
          <div className="grid grid-rows-2"><p>Gitlab</p><button className="btn btn-disabled mx-4">use</button></div>
          <div className="grid grid-rows-2"><p>Jira</p><button className="btn btn-disabled mx-4">use</button></div>
          <div className="grid grid-rows-2"><p>Trello</p><button className="btn btn-disabled mx-4">use</button></div>
        </div>
        {integration == Mode.Github ? <>
          <br />
          Github Settings:
          <form onSubmit={(event: React.FormEvent) => {
            mode.setMode(Mode.Github);
            setIntegration(Mode.Github);
            event.preventDefault();
          }}>
            <label>
              Repository:
              <input type="text" name="repo" className="input input-bordered input-sm w-full max-w-xs mx-1"
                defaultValue="https://github.com/flyck/dummy-planning"
                onChange={(event: React.ChangeEvent<HTMLInputElement>) => { localStorage.setItem("github_repo", event.target.value) }}
              />
            </label>
            <br />
            <label>
              Token:
              <input type="password" name="token" className="input input-bordered input-sm w-full max-w-xs mx-1 "
                defaultValue={localStorage.getItem("token") ?? ""}
                onChange={(event: React.ChangeEvent<HTMLInputElement>) => { localStorage.setItem("token", event.target.value) }} />
            </label>
            <br />
            <input className="btn bg-green-600 mx-1 mt-2" type="submit" value="Activate" />
          </form>
        </> : undefined}
      </div>
      <div className="mt-6"></div>
      <b> Integration: Graphs</b>
      <p>None</p>
      <div className="mt-6"></div>
      <b>Display Options</b>
      <div className="form-control">
        <label className="cursor-pointer label">
          <span className="label-text">Display Decisions</span>
          <input type="checkbox" className="toggle toggle-accent" onClick={() => events.setDisplayDecisions(!events.displayDecisions)} defaultChecked={events.displayDecisions} />
        </label>
        <label className="cursor-pointer label">
          <span className="label-text">Display Epics</span>
          <input type="checkbox" className="toggle toggle-accent" onClick={() => epics.setDisplayEpics(!epics.displayEpics)} defaultChecked={epics.displayEpics} />
        </label>

      </div>
    </Modal >
  );
};

export default Settings;
