import { FullHypothesis, Hypothesis, HypothesisStatus, User } from "~/types/hypotheses";
import { Iteration } from "~/types/iterations";
import { Kpi } from "~/types/kpis";
import { Release } from "~/types/releases";
import { Sprint } from "~/types/sprints";

export const users: User[] = [
  {
    userId: 123,
    name: "Felix",
    avatar: "https://avatars.githubusercontent.com/u/11775168?s=96&v=4"
  },
  {
    userId: 42,
    name: "Hendrik",
    avatar: "https://avatars.githubusercontent.com/u/51416554?v=4"
  }
];

export const hypotheses: Hypothesis[] = [
  {
    id: "HYPO-3",
    title: "Clients need a download feature",
    status: HypothesisStatus.confirmed,
    description: `Given that clients can create collections on our site, it would probably make sense to have a
download feature. With such a feature, a client could save his data, and get much more use out of our site.`,
    result: `In the end this got confirmed, we do see that customers like and use this new feature.`,
    from: '2023-05-01',
    to: '2023-06-12',
    owner: 123
  },
  {
    id: "HYPO-4",
    title: "A download feature will lead to more bookings",
    status: HypothesisStatus.disconfirmed,
    description: `The experienced "snappiness" of a site, has been proven to have a positive impact on the user experience. A faster experience on our site would lead to happy customers. Happier customers would let to better reviews and referals, which would in turn lead to more bookings.`,
    result: `In the end customer data shows that this is not so, atleast not in the short term after release.`,
    from: '2023-05-18',
    to: '2023-06-24',
    owner: 123
  },
  {
    id: "HYPO-5",
    title: "Clients need upload previews",
    status: HypothesisStatus.dropped,
    description: `Customers might like a news display on our site. Given these news, they could stay up to date, they would feel like being informed, and we would have to send out less emails. By having news directly on the site, this could drive more regular traffic, and could make visitors coming back. A more informed customer is a happier customer.`,
    result: `Dropped because it seems unimportant.`,
    from: '2023-05-01',
    to: '2023-06-24',
  },
  {
    id: "HYPO-123",
    title: "Clients want to connect with us via API",
    status: HypothesisStatus.open,
    description: "Clients want to connect with us via API",
    from: '2023-06-01',
    owner: 42,
  }
];

export function getFullHypotheses() {
  return hypotheses.map((hypo) => {
    let user: User | undefined = undefined
    if (hypo.owner) {
      user = users.filter((user) => user.userId == hypo.owner)[0]
    }
    const fullHypo: FullHypothesis = {
      id: hypo.id,
      title: hypo.title,
      status: hypo.status,
      owner: user,
      description: hypo.description,
      result: hypo.result,
      from: hypo.from,
      to: hypo.status === "OPEN" ? undefined : hypo.to
    }
    return fullHypo
  })
}

export const releases: Release[] = [
  {
    date: '2023-05-24',
    name: "Downloads MVP",
    version: "v1.0.0",
    url: new URL("https://google.com")
  },
  {
    date: '2023-06-04',
    name: "Download Filters",
    version: "v1.1.1",
    url: new URL("https://google.com")
  },
  {
    date: '2023-06-12',
    name: "Fix Downloads",
    version: "v1.1.2",
    url: new URL("https://google.com")
  },
  {
    date: '2023-06-24',
    name: "Add Upload Previews",
    version: "v1.2.5",
    url: new URL("https://google.com")
  },
];


export const sprints: Sprint[] = [
  {
    name: "Sprint 1",
    from: "2023-05-01",
    to: "2023-05-14",
    epics: [
      "Customer Downloads",
      "Design",
      "Research",
    ],
    issues: []
  },
  {
    name: "Sprint 2",
    from: "2023-05-15",
    to: "2023-05-28",
    epics: [
      "Customer Downloads",
      "Operations",
    ],
    issues: []
  },
  {
    name: "Sprint 3",
    from: "2023-05-29",
    to: "2023-06-11",
    epics: [
      "Customer Downloads",
      "Operations",
    ],
    issues: []
  },
  {
    name: "Sprint 4",
    from: "2023-06-12",
    to: "2023-06-25",
    epics: [
      "Refactoring",
      "Operations",
      "Previews Epic",
    ],
    issues: []
  },
  {
    name: "Sprint 5",
    from: "2023-06-26",
    to: "2023-07-09",
    epics: [
    ],
    issues: []
  },
  {
    name: "Sprint 6",
    from: "2023-07-10",
    to: "2023-07-23",
    epics: [
    ],
    issues: []
  },
  {
    name: "Sprint 7",
    from: "2023-07-24",
    to: "2023-08-06",
    epics: [
    ],
    issues: []
  }
]

export const kpis: Kpi[] = [
  {
    name: "Clicks",
    description: "Clicks on the page and really anywhere should be increased. Target is at 300/day on our product. If we have more product interaction, people get more use out of it and stay longer on the site, which helps us get more bookings.",
    from: '2023-05-01',
    to: '2023-05-24'
  },
  {
    name: "Reviews",
    description: "Reviews should be as many as good as possible. Target of 10 5star reviews per week. Having good reviews will give us many more bookings and more money.",
    from: '2023-05-01',
    to: '2023-06-25'
  },
  {
    name: "Conversions",
    description: "What we really need is conversions. Target is 300 / month.",
    from: '2023-06-01',
    to: '2023-06-25',
  }
]


export const iterations: Iteration[] = [
  {
    id: "IT-0",
    url: "https://...",
    type: "kickoff",
    description: "Kickoff for Dashboard Improvement Initiative. New Hypotheses created. Let's tackle them!",
    date: "2023-05-01",
    relatedItems: [
      { type: "hypothesis", url: "https://google.com", text: "Hypothesis: Clients need a download feature" },
      { type: "hypothesis", url: "https://google.com", text: "Hypothesis: Clients need upload previews" },
    ]
  },
  {
    id: "IT-1",
    url: "https://...",
    type: "iteration",
    description: "Learned a few technical challenges for the downloads. We could improve the download speed further but it talking to the customer, what seems most important is to add filters. Decision: Focus on filters next.",
    date: "2023-05-24",
    relatedItems: [
      { type: "kpi", url: "https://google.com", text: "Clicks increased by a lot, especially when interviewing the customer. Decided that clicks are too flaky of a metric and we should move to reviews instead." },
      { type: "release", url: "https://google.com", text: "MVP Release" },
      { type: "feedback", url: "https://google.com", text: "Interviewed a customer and he liked the new feature" }
    ]
  },
  {
    id: "IT-2",
    url: "https://...",
    type: "iteration",
    description: "Great news on the download feature, we could confirm our main hypothesis! Let's see if this will also lead to more bookings.",
    date: "2023-06-12",
    relatedItems: [
      { type: "feedback", url: "https://google.com", "text": "Customers told us they love the download feature and it was dearly needed." },
      { type: "release", url: "https://google.com", "text": "Fix Downloads" },
      { type: "kpi", url: "https://google.com", text: "Reviews. Got a positve review from one excited customers who is impressed by us moving fast." },
      { type: "hypothesis", url: "https://google.com", text: "Confirmed the hypothesis that customers need a download feature." }
    ]
  },
  {
    id: "IT-3",
    url: "https://...",
    type: "iteration",
    description: "Looking at the metrics we could update a few of our hypothesis.",
    date: "2023-06-24",
    relatedItems: [
      { type: "feedback", url: "https://google.com", "text": "Looking at the metrics the new feature got almost no usage." },
      { type: "release", url: "https://google.com", "text": "Upload Previews" },
      { type: "hypothesis", url: "https://google.com", "text": "Observing the metrics the bookings didnt really increase. This disconfirms the hypothesis." },
      { type: "hypothesis", url: "https://google.com", "text": "Dropped the client upload hypothesis." }
    ]
  }
]
