import { useEffect, useState } from "react"
import Modal from '../layouts/modal';
import RootModal from "./helper/modal"
import Link from "next/link";
import userImg from "~/assets/user.png"
import { calculateTimelinePosition, isRelevantDate } from "~/utils/timeline";
import { FullHypothesis, HypothesisStatus } from "~/types/hypotheses";
import { DateRange } from "~/types/general";
import { getFullHypotheses } from "./mockData";
import { Mode } from "./settings";
import { GithubService } from "~/services/github";

// with deadlines it is a bit full, TODO reconsider deadline visibility
const deadlines: Deadline[] = []
// const deadlines: Deadline[] = [{
//   title: 'Regulatory Change',
//   date: '2023-06-12',
//   description: 'Whatever we prepared until this point will',
//   affectedHypotheses: ["HYPO-4"]
// }]

type Deadline = {
  title: string
  date: string
  description: string
  affectedHypotheses?: string[]
}

interface CoolButtonProps {
  hypothesis: FullHypothesis
  openModal: (content: FullHypothesis) => void
  dateRange: DateRange
}

const CoolButton: React.FC<CoolButtonProps> = (props) => {
  const startPosition = calculateTimelinePosition(props.dateRange.startDate, props.dateRange.endDate, props.hypothesis.from)
  const endPosition = calculateTimelinePosition(props.dateRange.startDate, props.dateRange.endDate, props.hypothesis.to)
  const width = endPosition - startPosition

  return <>
    <div className="relative flex h-auto justify-between normal-case btn btn-primary btn-s" onClick={() => props.openModal(props.hypothesis)}
      style={{ left: `${startPosition}%`, width: `${width}%` }}
    >
      {
        (props.hypothesis.owner) ? <span className="">
          <img src={props.hypothesis.owner.avatar} alt={props.hypothesis.owner.name} className="hidden md:block w-7 h-7 rounded-full"></img>
        </span> : undefined
      }
      <div className="flex-1 text-left ml-2">{props.hypothesis.title}</div>
      <div className={`badge text-center ${statusColor(props.hypothesis.status)}`}>{props.hypothesis.status}</div>
    </div>
  </>
}


interface HypoProps {
  dateRange: DateRange
  mode: {
    mode: Mode
    setMode: (mode: Mode) => void
  }
}


const Hypotheses: React.FC<HypoProps> = ({ dateRange: { startDate, endDate }, mode }) => {
  const [activeModal, setActiveModal] = useState<FullHypothesis>();
  const [menuOpen, setMenuOpen] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(true);
  const [hypotheses, setHypotheses] = useState<FullHypothesis[]>();
  const [oldMode, setOldMode] = useState<Mode>(mode.mode)

  const handleOpenModal = (content: FullHypothesis | undefined) => {
    setActiveModal(content);
  };

  const handleCloseModal = () => {
    setActiveModal(undefined);
  };

  useEffect(() => {
    async function loadData() {
      console.log(mode)
      if (mode.mode == Mode.Mock) {
        setHypotheses(getFullHypotheses())
      }
      if (mode.mode == Mode.Github) {
        console.log("Loading from github")
        try {
          const githubService = new GithubService()
          const hypos = await githubService.getHypotheses({ startDate, endDate })
          setHypotheses(hypos)
          console.log()
        } catch (e) {
          console.error(e)
        }
      }
      setLoading(false)
    }

    if (loading) { loadData() } /* eslint-disable-line @typescript-eslint/no-floating-promises */
    if (oldMode != mode.mode) { console.log("Mode changed, must load again!"); setLoading(true); loadData(); setOldMode(mode.mode) }    /* eslint-disable-line @typescript-eslint/no-floating-promises */
  }, [mode.mode])

  // updates currently only the mockdata
  function updateHypothesis(id: string, status: HypothesisStatus) {
    hypotheses?.filter((hypo) => hypo.id === id).forEach((hypo) => {
      hypo.status = status
    })
  }

  return <>
    <div className="flex">
      <div className="container mx-auto">
        <div className="relative flex flex-col gap-2">
          {deadlines.map((deadline) => {
            const startPosition = calculateTimelinePosition(startDate, endDate, deadline.date)
            return <div key={deadline.title} className="relative" style={{ left: `${startPosition}%` }}>
              💀
            </div>
          }
          )}
        </div>
        <div className="relative flex flex-col gap-2">
          {hypotheses?.filter((hypo) => isRelevantDate(startDate, endDate, hypo.from, hypo.to)).map((hypo) => {
            const fullHypo: FullHypothesis = {
              id: hypo.id,
              title: hypo.title,
              url: hypo.url,
              status: hypo.status,
              owner: hypo.owner,
              description: hypo.description,
              result: hypo.result,
              from: hypo.from,
              to: hypo.status === "OPEN" ? undefined : hypo.to
            }
            return <CoolButton key={hypo.title} hypothesis={fullHypo} dateRange={{ startDate, endDate }} openModal={() => handleOpenModal(fullHypo)} />
          })}
        </div>
      </div>
    </div>
    <RootModal>
      {activeModal && (
        <Modal
          bgColor="bg-white"
          textColor="text-gray-900"
          onClose={handleCloseModal}
          title={"Hypothesis: " + activeModal.title}
          titleLink={activeModal.url ? activeModal.url.toString() : undefined}
          titleLinkText="Issue"
        >
          {activeModal.url ? <Link target="_blank" href={activeModal.url || ""} className="text-blue-900">Issue</Link> : undefined}
          <div className="grid grid-cols-4">

            <div className="col-span-3">
              <div className="">
                Description:
                <p>{activeModal.description}</p>
              </div>
              <div className="mt-4 flex flex-col">
                <label tabIndex={0} className={`badge text-center ${statusColor(activeModal.status)}`} onClick={() => { setMenuOpen(!menuOpen) }}>{activeModal.status}</label>
                {menuOpen && (
                  <div className="dropdown dropdown-bottom dropdown-open">
                    <ul
                      className="p-2 shadow menu dropdown-content z-[1] bg-base-100 rounded-box w-52"
                    >
                      {Object.values(HypothesisStatus).map((key, value) => {
                        return <li key={key}>
                          <div key={key} className={`badge text-center ${statusColor(key)}`} onClick={() => {
                            activeModal.status = key; updateHypothesis(activeModal.id, key); setMenuOpen(false)
                          }}>{key}</div>
                        </li>
                      })}
                    </ul>
                  </div>
                )}
              </div>
              {activeModal.result ? <div className="mt-4">
                Result:
                <p>{activeModal.result}</p>
              </div> : undefined
              }
              <div className="mt-4">
                {activeModal.status != "OPEN" ? <p>Active from {activeModal.from} to {activeModal.to}</p> : undefined}
                {activeModal.status == "OPEN" ? <p>Open since {activeModal.from}</p> : undefined}
              </div>

            </div>
            <div className="col-span-1 text-center">
              <img
                src={activeModal.owner ? activeModal.owner.avatar : userImg.src}
                alt={activeModal.owner ? activeModal.owner.name : "unassigned"}
                className="relative rounded-full w-24 h-24 mx-auto"></img>
              {activeModal.owner ? "Investigator: " + activeModal.owner.name : "Unassigned"}
            </div>
          </div>
        </Modal>
      )}
    </RootModal >
  </>
}

function statusColor(status: HypothesisStatus) {
  switch (status) {
    case (HypothesisStatus.confirmed): return "bg-green-300 text-gray-900";
    case (HypothesisStatus.disconfirmed): return "bg-green-200 text-gray-900";
    case (HypothesisStatus.dropped): return "text-gray-900 bg-gray-300";
    case (HypothesisStatus.open): return "bg-red-500 text-gray-900";
  }
}

export default Hypotheses
