import React, { useEffect, useState } from 'react';
import Modal from '../layouts/modal';
import RootModal from './helper/modal';
import { Epic, Sprint } from '~/types/sprints';
import { DateRange } from '~/types/general';
import { calculateTimelinePosition, isRelevantDate } from '~/utils/timeline';
import { Mode } from './settings';
import { sprints as mockSprints } from '~/components/mockData'
import { GithubService } from '~/services/github';
import Link from 'next/link';

interface SprintProps {
  dateRange: DateRange
  displayEpics: boolean
  mode: {
    mode: Mode
    setMode: (mode: Mode) => void
  }
}

interface SprintButtonProps {
  sprint: Sprint
  openSprintModal: (content: Sprint) => void
  openEpicModal: (content: Epic) => void
  dateRange: DateRange
  displayEpics: boolean
}

const SprintButton: React.FC<SprintButtonProps> = (props) => {
  const startPosition = calculateTimelinePosition(props.dateRange.startDate, props.dateRange.endDate, props.sprint.from)
  const endPosition = calculateTimelinePosition(props.dateRange.startDate, props.dateRange.endDate, props.sprint.to)
  const width = endPosition - startPosition

  return <div className="absolute flex flex-col" style={{ left: `${startPosition}%`, width: `${width}%` }
  }>
    <div className="btn btn-primary btn-s" onClick={() => props.openSprintModal(props.sprint)}>
      {props.sprint.name}
    </div>
    {props.displayEpics ? props.sprint.epics.map((epic) => (
      <button key={epic} className="h-auto btn btn-s bg-base-800 mt-1 ml-2 mr-2 normal-case"
        onClick={() => props.openEpicModal(epic)}>
        {epic}
      </button>
    )) : undefined}

  </div >
}


const Sprints: React.FC<SprintProps> = ({ dateRange: { startDate, endDate }, displayEpics, mode }) => {
  const [sprints, setSprints] = useState<Sprint[]>();
  const [activeSprintModal, setActiveSprintModal] = useState<Sprint>();
  const [activeEpicModal, setActiveEpicModal] = useState<Epic>();
  const [loading, setLoading] = useState<boolean>(true);
  const [oldMode, setOldMode] = useState<Mode>(mode.mode)

  const handleOpenSprintModal = (content: Sprint | undefined) => {
    setActiveSprintModal(content);
  };

  const handleCloseSprintModal = () => {
    setActiveSprintModal(undefined);
  };

  const handleOpenEpicModal = (content: Epic | undefined) => {
    setActiveEpicModal(content);
  };

  const handleCloseEpicModal = () => {
    setActiveEpicModal(undefined);
  };

  useEffect(() => {
    async function loadData() {
      console.log(mode)
      if (mode.mode == Mode.Mock) {
        setSprints(mockSprints)
      }
      if (mode.mode == Mode.Github) {
        console.log("Loading sprints from github")
        try {
          const githubService = new GithubService()
          const githubSprints = await githubService.getSprints({ startDate, endDate })
          console.log("GithubSprints: ", JSON.stringify(githubSprints))
          setSprints(githubSprints)
        } catch (e) {
          console.error(e)
        }
      }
      setLoading(false)
    }

    if (loading) { loadData() } /* eslint-disable-line @typescript-eslint/no-floating-promises */
    if (oldMode != mode.mode) { console.log("Mode changed, must load again!"); setLoading(true); loadData(); setOldMode(mode.mode) }    /* eslint-disable-line @typescript-eslint/no-floating-promises */
  }, [mode.mode])

  return <>
    <div className="flex">
      <div className="container mx-auto">

        <div className="relative">
          {sprints?.filter((sprint) => isRelevantDate(startDate, endDate, sprint.from, sprint.to)).map((sprint, index) => {
            return <>
              <SprintButton
                key={index}
                sprint={sprint}
                dateRange={{ startDate, endDate }}
                openSprintModal={handleOpenSprintModal}
                openEpicModal={handleOpenEpicModal}
                displayEpics={displayEpics}
              />
            </>
          })}
        </div>
      </div>
    </div>
    <div className="z-0">
      <RootModal>
        {activeSprintModal && (
          <Modal
            bgColor="bg-white"
            textColor="text-gray-900"
            onClose={handleCloseSprintModal}
            title={activeSprintModal.name}
          >
            <p>Sprint goal: This was our sprint goal</p>
            <br />
            <h2>Issues:</h2>
            <p><ul>{activeSprintModal.issues?.map((issue, index) => (
              <li key={index}>- {issue.title}<Link href={issue.url}>(issue)</Link></li>
            ))}</ul></p>
            <br />
            <h2>Active Epics:</h2>
            <p><ul>{activeSprintModal.epics?.map((epic) => (
              <li key={epic}>- {epic}</li>
            ))}</ul></p>
          </Modal >
        )}
        {
          activeEpicModal && (
            <Modal
              bgColor="bg-white"
              textColor="text-gray-900"
              onClose={handleCloseEpicModal}
              title={"Epic: " + activeEpicModal}
            >
              <p><b>Description</b>:</p>
              <p>This is the most epic epic there has ever been!</p>
              <br />
              Active from ... to ... (?)
              <br />
              <h2>Active in Sprints: ?</h2>
            </Modal>
          )
        }
      </RootModal>
    </div >

  </>


}

export default Sprints
