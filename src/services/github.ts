import { Octokit } from "@octokit/core";
import { DateRange } from "~/types/general";
import { FullHypothesis, HypothesisStatus } from "~/types/hypotheses";
import { Kpi } from "~/types/kpis";
import { Sprint } from "~/types/sprints";
import { getEndDate } from "~/utils/timeline";

type HypoResponse = {
  repository: {
    issues: {
      nodes: {
        id: number,
        title: string,
        body: string,
        url: URL,
        assignees: {
          nodes: {
            id: number
            name: string
            avatarUrl: string
          }[]
        }
        projectItems: {
          nodes: {
            start: { date: string }
            end: { date: string }
            hypoStatus: { name: string }
            sprint?: { duration: string, startDate: string }
          }[]
        }
      }[]
    }
  }
}

type SprintResponse = {
  repository: {
    issues: {
      nodes: {
        id: number,
        title: string,
        body: string,
        url: URL,
        projectItems: {
          nodes: {
            sprint?: { duration: string, startDate: string, title: string, id: string }
          }[]
        }
      }[]
    }
  }
}


export class GithubService {
  private kit: Octokit
  private owner: string
  private name: string

  constructor() {
    this.kit = new Octokit({
      auth: localStorage.getItem("token")
    })
    const repo = localStorage.getItem("github_repo")
    if (!repo) throw new Error("repo localstorage not set!")
    const owner = repo.split("/")[3]
    const name = repo.split("/")[4]
    if (!owner || !name) throw new Error("Couldnt load github connection")
    this.owner = owner
    this.name = name
  }

  async getHypotheses(dateRange: DateRange) {
    const response = await this.kit.graphql(`
query {
  repository(owner: "${this.owner}", name:"${this.name}") {
    issues(labels:"hypothesis", first:100) {
      nodes {
        id
        url
        title
        body
        assignees(first: 1) {
          nodes {
            avatarUrl
            name
            id
          }
        }
        projectItems(first:10) {
          nodes {
            id
            sprint: fieldValueByName(name:"Sprint") {
              ... on 	ProjectV2ItemFieldIterationValue{
                id
                duration
                startDate
              }
            }
            start: fieldValueByName(name:"Start") {
              ... on 	ProjectV2ItemFieldDateValue{
                id
                date
              }
            }
            end: fieldValueByName(name:"End") {
              ... on 	ProjectV2ItemFieldDateValue{
                id
                date
              }
            }
            hypoStatus: fieldValueByName(name:"Hypothesis Status") {
              ... on 	ProjectV2ItemFieldSingleSelectValue{
                id
                name
              }
            }
          }
        }
      }
    }
  }
}
`)
    console.log(response)
    const issues = (response as unknown as HypoResponse).repository.issues.nodes // eslint-disable-line @typescript-eslint/no-unnecessary-type-assertion

    const fullHypos: FullHypothesis[] = issues.map((issue) => ({
      id: issue.id.toString(),
      title: issue.title,
      result: "asd",
      status: (issue.projectItems.nodes.length > 0 ? issue.projectItems.nodes[0]!.hypoStatus.name : "").toUpperCase() as HypothesisStatus,
      description: issue.body || "",
      url: issue.url,
      owner: issue.assignees.nodes.length > 0 ? {
        avatar: issue.assignees.nodes[0]!.avatarUrl,
        name: issue.assignees.nodes[0]!.name,
        userId: issue.assignees.nodes[0]!.id,
      } : undefined,
      from: (issue.projectItems.nodes.length > 0 ? issue.projectItems.nodes[0]!.start.date : "asd"),
      to: (issue.projectItems.nodes.length > 0 ? issue.projectItems.nodes[0]!.end?.date : undefined),
    }))

    console.log(fullHypos)

    return fullHypos
  }

  async getKpis(dateRange: DateRange) {
    const response = await this.kit.graphql(`
query {
  repository(owner: "${this.owner}", name:"${this.name}") {
    issues(labels:"kpi", first:100) {
      nodes {
        id
        url
        title
        body
        assignees(first: 1) {
          nodes {
            avatarUrl
            name
            id
          }
        }
        projectItems(first:10) {
          nodes {
            id
            sprint: fieldValueByName(name:"Sprint") {
              ... on 	ProjectV2ItemFieldIterationValue{
                id
                duration
                startDate
              }
            }
            start: fieldValueByName(name:"Start") {
              ... on 	ProjectV2ItemFieldDateValue{
                id
                date
              }
            }
            end: fieldValueByName(name:"End") {
              ... on 	ProjectV2ItemFieldDateValue{
                id
                date
              }
            }
            hypoStatus: fieldValueByName(name:"Hypothesis Status") {
              ... on 	ProjectV2ItemFieldSingleSelectValue{
                id
                name
              }
            }
          }
        }
      }
    }
  }
}
`)
    console.log(response)
    const issues = (response as unknown as HypoResponse).repository.issues.nodes // eslint-disable-line @typescript-eslint/no-unnecessary-type-assertion

    const kpis: Kpi[] = issues.map((issue) => ({
      name: issue.title,
      description: issue.body || "",
      url: issue.url,
      from: (issue.projectItems.nodes.length > 0 ? issue.projectItems.nodes[0]!.start.date : "asd"),
      to: (issue.projectItems.nodes.length > 0 ? issue.projectItems.nodes[0]!.end?.date : undefined),
    }))

    console.log(kpis)

    return kpis
  }

  async getSprints(dateRange: DateRange) {
    const response = await this.kit.graphql(`
query {
  repository(owner: "${this.owner}", name:"${this.name}") {
    issues(first:100) {
      nodes {
        id
        url
        title
        body
        projectItems(first:1) {
          nodes {
            id
            sprint: fieldValueByName(name:"Sprint") {
              ... on 	ProjectV2ItemFieldIterationValue{
                id
                duration
                startDate
                title
              }
            }
          }
        }
      }
    }
  }
}
`)
    console.log("Raw Sprint Response: ", JSON.stringify(response, null, 4))
    const issues = (response as SprintResponse).repository.issues.nodes
    const sprints: { [key: string]: Sprint } = {}
    issues.forEach(issue => {
      issue.projectItems.nodes.forEach(node => {
        console.log("Sprint title: ", node?.sprint?.title)
        if (!node?.sprint?.title) { return }
        if (sprints[node.sprint.title] && Object.entries(!sprints[node.sprint.title]).length == 0) {
          sprints[node.sprint.title] = {
            name: node.sprint.title,
            from: node.sprint.startDate,
            to: getEndDate(node.sprint.startDate, parseInt(node.sprint.duration)),
            epics: [],
            issues: [{
              title: issue.title,
              url: issue.url
            }]
          }
        } else {
          sprints[node?.sprint?.title]?.issues.concat({
            title: issue.title,
            url: issue.url
          })

        }

      })
    })
    console.log("Mapped Response: ", Object.values(sprints))
    return Object.values(sprints)
  }
}
