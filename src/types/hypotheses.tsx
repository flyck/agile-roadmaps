export enum HypothesisStatus {
  open = "OPEN",
  confirmed = "CONFIRMED",
  disconfirmed = "DISCONFIRMED",
  dropped = "DROPPED"
}
export type Hypothesis = {
  id: string;
  title: string;
  status: HypothesisStatus;
  description: string;
  result?: string;
  from: string;
  to?: string;
  owner?: number;
};
export type FullHypothesis = {
  id: string;
  title: string;
  status: HypothesisStatus;
  description: string;
  url?: URL;
  result?: string;
  from: string;
  to?: string;
  owner?: User;
};
export type User = {
  userId: number;
  name: string;
  avatar?: string;
};
