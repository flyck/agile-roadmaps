export type Kpi = {
  name: string
  description: string
  url?: URL
  from: string
  to?: string
}
