export interface Release {
  date: string
  name: string
  version: string
  url: URL
}
