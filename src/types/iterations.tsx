export type Iteration = {
  id: string
  url: string
  type: "kickoff" | "iteration"
  description: string
  date: string
  relatedItems: {
    type: "kpi" | "hypothesis" | "plan" | "release" | "feedback"
    url: string
    text?: string
  }[]
}
