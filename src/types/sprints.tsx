export type Sprint = {
  name: string
  from: string
  to: string
  epics: Epic[]
  issues: Issue[]
}

export type Epic = string

export type Issue = {
  title: string
  url: URL
}
