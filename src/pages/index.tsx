import React, { useEffect, useState } from 'react';
import { type NextPage } from "next";
import Head from "next/head";
import Events from "~/components/events";
import Hypotheses from "~/components/hypothesis";
import KpiComponent from "~/components/kpis";
import Sprints from "~/components/sprints";
import Releases from "~/components/releases";
import gitlab from "~/assets/gitlab-mark.svg"
import Image from 'next/image';

import { api } from "~/utils/api";
import Settings, { Mode } from "~/components/settings";
import Link from 'next/link';
import { getFullHypotheses, releases, sprints } from '~/components/mockData';
import Iterations from '~/components/iterations';
import { themeChange } from 'theme-change'
import { formatDate } from '~/utils/timeline';
import { SprintNavigationButton } from '~/components/navigation/buttons';

const Home: NextPage = () => {
  const hello = api.example.hello.useQuery({ text: "from tRPC" });
  const [displayDecisions, setDisplayDecisions] = useState(false)
  const [displayEpics, setDisplayEpics] = useState(false)
  const [dateRange, setDateRange] = useState({ startDate: '2023-05-01', endDate: '2023-06-25' })
  const [mode, setMode] = useState(Mode.Mock)

  function changeDateRange(input: "increase" | "decrease", days = 7) {
    const delta = input == "increase" ? days : -days

    const currentStartDate = new Date(dateRange.startDate)
    const newStartDate = currentStartDate
    newStartDate.setDate(currentStartDate.getDate() + delta)

    const currentEndDate = new Date(dateRange.endDate)
    const newEndDate = currentEndDate
    newEndDate.setDate(currentEndDate.getDate() + delta)

    console.log({ startDate: formatDate(newStartDate), endDate: formatDate(newEndDate) })
    setDateRange({ startDate: formatDate(newStartDate), endDate: formatDate(newEndDate) })
  }

  function zoomDateRange(input: "in" | "out", days = 7) {
    const currentStartDate = new Date(dateRange.startDate)
    const currentEndDate = new Date(dateRange.endDate)

    console.log(currentEndDate.getTime())
    if (input == "in" && (currentEndDate.getTime() - currentStartDate.getTime()) / (1000 * 60 * 60 * 24) < 14) {
      console.log("Minimum reached!")
      return
    }

    const newStartDate = currentStartDate
    newStartDate.setDate(currentStartDate.getDate() + (input == "in" ? days : -days))

    const newEndDate = currentEndDate
    newEndDate.setDate(currentEndDate.getDate() + (input == "in" ? -days : days))

    setDateRange({ startDate: formatDate(newStartDate), endDate: formatDate(newEndDate) })
  }

  useEffect(() => {
    themeChange(false)
    // 👆 false parameter is required for react project
  }, [])

  return (
    <>
      <Head>
        <title>Agile Roadmaps</title>
        <meta name="description" content="Generated by create-t3-app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className="flex min-h-screen flex-col bg-gradient-to-b from-black-100 to-base-100">
        <div className="navbar bg-base-300">
          <div className="flex-1 px-2 lg:flex-none">
            <a className="text-lg font-bold">Agile Roadmaps (MVP)</a>
          </div>
          <div className="flex justify-end flex-1 px-2">
            <div className="flex items-stretch">
              {/*https://github.com/saadeghi/theme-change */}
              <select className="z-0 items-center select select-bordered" data-choose-theme>
                <option value="winter">Light</option>
                <option value="night">Dark</option>
                <option value="corporate">Corporate</option>
                <option value="dracula">Dracula</option>
                <option value="valentine">Valentine</option>
                <option value="pastel">Pastel</option>
                <option value="wireframe">Wireframe</option>
              </select>
              <div className="z-50 w-12">
                <Link href="https://gitlab.com/flyck/agile-roadmaps">
                  <Image
                    priority
                    src={gitlab as string}
                    alt="Gitlab logo"
                    width="50"
                  />
                </Link>
              </div>
              <div className="z-50 w-12">
                <Settings events={{ setDisplayDecisions, displayDecisions }} epics={{ setDisplayEpics, displayEpics }} mode={{ setMode, mode }} />
              </div>
            </div>
          </div>
        </div>
        <div className="mt-6"></div>
        <div id="modal-root" className="z-50"></div>
        <div className="flex flex-col">
          <SprintNavigationButton type="decrease" displayEpics={displayEpics} onClick={() => changeDateRange("decrease")} />
          <div className="mx-auto max-w-screen-xl w-11/12 xl:w-8/12 px-1">
            <KpiComponent dateRange={dateRange} mode={{ setMode, mode }} />
            <div className="mt-4"></div>
            <Hypotheses dateRange={dateRange} mode={{ setMode, mode }} />
            <div className="mt-4"></div>
            {/*<Events dateRange={dateRange} displayDecisions={displayDecisions} />*/}
            <Iterations dateRange={dateRange} mode={{ setMode, mode }} />
            <div className="mt-2"></div>
            <Releases dateRange={dateRange} releases={releases} />
            <div className="mt-0"></div>
            <Sprints dateRange={dateRange} displayEpics={displayEpics} mode={{ setMode, mode }} />
            <br />
          </div>
          <div className={"absolute right-12 top-40 hidden sm:flex flex-col justify-end " + (displayEpics ? "mb-32" : "mb-12")}>
            <button className="btn btn-s w-16 bg-content mx-8" onClick={() => zoomDateRange("out")}>-</button>
          </div>
          <div className={"absolute right-12 top-28 hidden sm:flex flex-col justify-end " + (displayEpics ? "mb-32" : "mb-12")}>
            <button className="btn btn-s w-16 bg-content mx-8" onClick={() => zoomDateRange("in")}>+</button>
          </div>
          <SprintNavigationButton type="increase" displayEpics={displayEpics} onClick={() => changeDateRange("increase")} />
        </div >
      </main >
      <footer>
        <div className="navbar bg-base-200 grid justify-items-end">
          <small className="">&copy; Copyright 2023, @flyck</small>
        </div>
      </footer >
    </>
  );
};

export default Home;
