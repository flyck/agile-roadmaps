import Link from 'next/link';
import React, { useEffect, type ReactNode } from 'react';

type ModalLayoutProps = {
  bgColor: string;
  textColor: string;
  borderColor?: string;
  onClose: () => void;
  title: string;
  titleLink?: string;
  titleLinkText?: string;
  children: ReactNode;
};

const ModalLayout = ({ bgColor, textColor, borderColor, onClose, title, titleLink, titleLinkText, children }: ModalLayoutProps) => {
  useEffect(() => {
    const handleOutsideClick = (event: MouseEvent) => {
      if (event.target instanceof HTMLElement && event.target.classList.contains('fixed')) {
        onClose();
      }
    };

    const handleEscapeKey = (event: KeyboardEvent) => {
      if (event.key === 'Escape') {
        onClose();
      }
    };

    window.addEventListener('click', handleOutsideClick);
    window.addEventListener('keydown', handleEscapeKey);

    return () => {
      window.removeEventListener('click', handleOutsideClick);
      window.removeEventListener('keydown', handleEscapeKey);
    };
  }, [onClose]);

  return (
    <div
      className={`fixed inset-0 flex items-center justify-center bg-opacity-0 bg-base-200`}
    >
      <div className={`rounded-lg p-4 bg-base-100 text-content ` + ((borderColor) ? ` border-4 ${borderColor} ` : ' border-4 border-neutral-focus ') + ` mx-2 md:w-2/3 max-w-2xl`}>
        <div className="flex justify-between">
          <h3 className="text-lg font-bold">{title} {titleLink ? <Link className="text-blue-900" target="_blank" href={titleLink}>({titleLinkText || "Link"})</Link> : undefined}</h3>
          <button onClick={onClose} className="">
            ✖
          </button>
        </div>
        <div className="mt-4">{children}</div>
      </div>
    </div >
  );
};

export default ModalLayout;
