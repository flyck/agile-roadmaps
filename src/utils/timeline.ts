export const getTotalDays = (start: string, end: string): number => {
  const oneDay = 24 * 60 * 60 * 1000; // Number of milliseconds in a day
  const startDate = new Date(start);
  const endDate = new Date(end);
  return Math.round(((endDate.getTime() - startDate.getTime()) / oneDay));
};

export const calculateTimelinePosition = (startDate: string, endDate: string, date?: string): number => {
  // allow for items that have no endDate
  if (!date) date = endDate;

  const totalDays = getTotalDays(startDate, endDate);
  const datePosition = getTotalDays(startDate, date);

  // Calculate position as a percentage
  const position = (datePosition / totalDays) * 100;

  if (position < 0) return 0
  if (position > 100) return 100
  return position
};

export function isRelevantDate(startDate: string, endDate: string, from: string, to?: string) {
  const dates = {
    startDate: new Date(startDate),
    endDate: new Date(endDate),
    from: new Date(from),
    to: to ? new Date(to) : undefined,
  }

  // At least one of the given dates has to overlap
  if (dates.from >= dates.startDate && dates.from <= dates.endDate) return true
  if (dates.to && dates.to >= dates.startDate && dates.to <= dates.endDate) return true
  if (dates.from <= dates.startDate && (!dates.to || dates.to >= dates.endDate)) return true

  return false
}


export function formatDate(d: Date) {
  return `${d.getFullYear()}-${d.getMonth() + 1}-${d.getDate()}`;
}

export function getEndDate(startDate: string, duration: number) {
  // Add 14 days to the current date
  const futureDate = new Date(startDate);
  futureDate.setDate(new Date(startDate).getDate() + duration);
  return formatDate(futureDate)
}
